package dam.androidisrael.phoneapp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import java.util.ArrayList;

import dam.androidisrael.phoneapp.models.Usuario;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatsUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatsUserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ChatsUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatsUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatsUserFragment newInstance(String param1, String param2) {
        ChatsUserFragment fragment = new ChatsUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats_user, container, false);
        RecyclerView rvChats = view.findViewById(R.id.rvChats);
        ArrayList<Usuario> chats = new ArrayList<>();
        Usuario user = new Usuario();
        user.setNombre("Manolo");
        chats.add(user);
        rvChats.setLayoutManager(new LinearLayoutManager(getContext()));
        rvChats.setAdapter(new AdapterChat(chats, getContext()));
        // Inflate the layout for this fragment
        return view;
    }

    @Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation a = new Animation() {};
        a.setDuration(0);
        return a;
        //return super.onCreateAnimation(transit, enter, nextAnim);
    }
}