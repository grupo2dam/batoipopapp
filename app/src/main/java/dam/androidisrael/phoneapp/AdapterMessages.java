package dam.androidisrael.phoneapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Random;

import dam.androidisrael.phoneapp.models.Message;

public class AdapterMessages extends RecyclerView.Adapter<AdapterMessages.MyViewHolder> {
    private ArrayList<Message> messages;
    private int position;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView text;
        private TextView hour;
        private Context context;
        public MyViewHolder(View view, Context context) {
            super(view);
            text = view.findViewById(R.id.messageText);
            hour = view.findViewById(R.id.messageHour);
            this.context = context;
        }
        public void bind(Message message){
            text.setText(message.getContent());
            hour.setText(message.getHour());
        }
    }

    public AdapterMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public void updateMessages(ArrayList<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(!messages.get(position).isMine()){
            return 1;
        }
        return 0;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout;
        if(viewType == 1){
            itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_recieved, parent, false);
        } else {
            itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_sent, parent, false);
        }
        return new MyViewHolder(itemLayout, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
}
