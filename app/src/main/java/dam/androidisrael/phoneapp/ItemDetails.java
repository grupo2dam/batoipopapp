package dam.androidisrael.phoneapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.NumberFormat;

import dam.androidisrael.phoneapp.models.Product;
import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public class ItemDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        setUI();
    }

    private void setUI() {
        getSupportActionBar().hide();
        ImageView backArrow = findViewById(R.id.backArrow);
        BatoipopArticulo product = (BatoipopArticulo)getIntent().getSerializableExtra("product");
        ImageView image = findViewById(R.id.imgProducto);
        TextView nombre = findViewById(R.id.tvNombreProducto);
        TextView precio = findViewById(R.id.tvPrecio);
        TextView descripcion = findViewById(R.id.tvDescripcion);
        FloatingActionButton chat = findViewById(R.id.fab);
        if((product.getUsuarioPublicar()==null || getUsuario()==null) || product.getUsuarioPublicar().getId()==getUsuario().getId()){
                chat.hide();
        }
        if(product.getUsuarioPublicar()!=null){
            FragmentUsuario usuario = FragmentUsuario.newInstance(product.getUsuarioPublicar().getNombre());
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.menuMain, usuario, null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }
        nombre.setText(product.getNombre());
        precio.setText(NumberFormat.getInstance().format(product.getPrecio())+"€");
        descripcion.setText(product.getDescripcion());
        Glide.with(getApplicationContext()).load(product.getImageURL()).into(image);
        backArrow.setOnClickListener((View v) -> { finish(); });
        chat.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), UserChat.class);
            intent.putExtra("id", product.getUsuarioPublicar().getId());
            startActivity(intent);
            finish();
        });
    }

    private BatoipopUsuario getUsuario() {
        String namePrefs = "BatoiPopPrefs";
        SharedPreferences myPreferences = null;
        String userLogged = null;
        try {
            myPreferences = EncryptedSharedPreferences.create(namePrefs,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    getApplicationContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
            userLogged = myPreferences.getString("usuarioLogged", null);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(userLogged!=null){
            Gson gson = new Gson();
            return gson.fromJson(userLogged, BatoipopUsuario.class);
        }
        return null;
    }
}