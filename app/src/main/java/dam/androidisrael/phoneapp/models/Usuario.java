package dam.androidisrael.phoneapp.models;

public class Usuario {
    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private String contasena;
    private float valoracion;

    public Usuario() {
    }

    public Usuario(int id, String nombre, String email, String telefono, String contasena, float valoracion) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.contasena = contasena;
        this.valoracion = valoracion;
    }

    public Usuario(String nombre, String email, String telefono, String contasena, float valoracion) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.contasena = contasena;
        this.valoracion = valoracion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getContasena() {
        return contasena;
    }

    public void setContasena(String contasena) {
        this.contasena = contasena;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }
}
