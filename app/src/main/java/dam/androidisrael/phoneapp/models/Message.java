package dam.androidisrael.phoneapp.models;

import java.io.Serializable;

public class Message implements Serializable {
    private String content;
    private String hour;
    private boolean mine;

    public Message(){};

    public Message(String content, String hour) {
        this.content = content;
        this.hour = hour;
    }

    public Message(String content, String hour, boolean mine) {
        this.content = content;
        this.hour = hour;
        this.mine = mine;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }
}
