package dam.androidisrael.phoneapp.models;

public class Valoracion {
    private int id;
    private float puntuacion;
    private String contenido;
    private Usuario usuario_valorado;
    private Usuario usuario_comentario;

    public Valoracion() {
    }

    public Valoracion(int id, float puntuacion, String contenido, Usuario usuario_valorado, Usuario usuario_comentario) {
        this.id = id;
        this.puntuacion = puntuacion;
        this.contenido = contenido;
        this.usuario_valorado = usuario_valorado;
        this.usuario_comentario = usuario_comentario;
    }

    public Valoracion(float puntuacion, String contenido, Usuario usuario_valorado, Usuario usuario_comentario) {
        this.puntuacion = puntuacion;
        this.contenido = contenido;
        this.usuario_valorado = usuario_valorado;
        this.usuario_comentario = usuario_comentario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(float puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Usuario getUsuario_valorado() {
        return usuario_valorado;
    }

    public void setUsuario_valorado(Usuario usuario_valorado) {
        this.usuario_valorado = usuario_valorado;
    }

    public Usuario getUsuario_comentario() {
        return usuario_comentario;
    }

    public void setUsuario_comentario(Usuario usuario_comentario) {
        this.usuario_comentario = usuario_comentario;
    }
}
