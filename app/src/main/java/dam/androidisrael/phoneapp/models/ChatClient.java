package dam.androidisrael.phoneapp.models;

import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.api.EncryptionService;

public abstract class ChatClient {
    private static final String hostName = "137.74.226.42";
    private static final int portNumber = 9090;
    private Socket socket;
    private Gson gson = new Gson();
    private boolean connected = false;

    public void closeSession(){
        try {
            socket.close();
            connected = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startSession(int idSender, int idReciever){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        //Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(hostName, portNumber);
                    connected = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try{
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader serverInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    out.println(idSender);
                    while(serverInput.readLine()==null){}
                    out.println(idReciever);
                    getMessages();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {

                }
                /*handler.post(new Runnable() {
                    @Override
                    public void run() {

                    }
                });*/
            }
        });
    }

    public void getMessages(){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        try {
            BufferedReader serverInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            executor.execute(new Runnable() {
            @Override
            public void run() {
                while (connected){
                    String input;
                    try {
                    if((input=serverInput.readLine())!=null||!input.equals("")){
                        Message message = gson.fromJson(EncryptionService.getIntance().decrypt(input), Message.class);
                        message.setMine(false);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                recieveMessage(message);
                            }
                        });
                    }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message message){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        //Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(EncryptionService.getIntance().encrypt(gson.toJson(message)));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*handler.post(new Runnable() {
                    @Override
                    public void run() {

                    }
                });*/
            }
        });
    }

    public abstract void recieveMessage(Message message);
}
