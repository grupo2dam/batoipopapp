package dam.androidisrael.phoneapp.pojos;
// Generated 16 feb. 2022 18:10:11 by Hibernate Tools 5.2.12.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * BatoipopCategoria generated by hbm2java
 */
public class BatoipopCategoria implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String nombre;
	private String descripcion;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;
	private Set<BatoipopArticulo> batoipopArticulos = new HashSet<BatoipopArticulo>(0);

	public BatoipopCategoria() {
	}

	public BatoipopCategoria(int id) {
		this.id = id;
	}

	public BatoipopCategoria(int id, String nombre, String descripcion, Integer createUid, Date createDate, Integer writeUid, Date writeDate, Set<BatoipopArticulo> batoipopArticulos) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.batoipopArticulos = batoipopArticulos;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public Set<BatoipopArticulo> getBatoipopArticulos() {
		return this.batoipopArticulos;
	}

	public void setBatoipopArticulos(Set<BatoipopArticulo> batoipopArticulos) {
		this.batoipopArticulos = batoipopArticulos;
	}

}
