package dam.androidisrael.phoneapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.NumberFormat;
import java.util.ArrayList;

import dam.androidisrael.phoneapp.models.Product;
import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<BatoipopArticulo> products;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagen;
        private TextView nombre;
        private TextView precio;
        private Context context;
        private BatoipopArticulo product;
        public MyViewHolder(View view, Context context) {
            super(view);
            imagen = view.findViewById(R.id.img);
            nombre = view.findViewById(R.id.tvName);
            precio = view.findViewById(R.id.tvPrice);
            this.context = context;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent details = new Intent(context, ItemDetails.class);
                    details.putExtra("product", product);
                    context.startActivity(details);
                }
            });
        }
        public void bind(BatoipopArticulo product){
            this.product = product;
            nombre.setText(product.getNombre());
            precio.setText(NumberFormat.getInstance().format(product.getPrecio())+"€");
            imagen.setTag(product.getImageURL());
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            imagen.setLayoutParams(params);
            imagen.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imagen.setAdjustViewBounds(true);
            GlideApp.with(context).load(product.getImageURL())
                    .dontTransform()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .centerInside()
                    .centerCrop()
                    .dontAnimate()
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .error(R.drawable.icon)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(imagen);


            //Glide.with(context).load(product.getImageURL()).into(imagen);
        }
    }

    public MyAdapter(ArrayList<BatoipopArticulo> products) {
        this.products = products;
    }

    public void updateProducts(ArrayList<BatoipopArticulo> products) {
        this.products = products;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_layout, parent, false);
        return new MyViewHolder(itemLayout, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

}
