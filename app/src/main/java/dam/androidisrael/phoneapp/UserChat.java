package dam.androidisrael.phoneapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import dam.androidisrael.phoneapp.models.ChatClient;
import dam.androidisrael.phoneapp.models.Message;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public class UserChat extends AppCompatActivity {
    private ArrayList<Message> messages = new ArrayList<>();
    private AdapterMessages adapterMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat);
        int id = getIntent().getIntExtra("id", 0);
        setUI(id);

    }

    private void setUI(int id) {
        getSupportActionBar().hide();
        EditText text = findViewById(R.id.etMessages);
        RecyclerView rvMessages = findViewById(R.id.rvMessages);
        adapterMessages = new AdapterMessages(messages);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvMessages.setLayoutManager(llm);
        rvMessages.setAdapter(adapterMessages);
        ChatClient chatClient = new ChatClient() {
            @Override
            public void recieveMessage(Message message) {
                messages.add(message);
                Log.e("R", ""+message.isMine());
                adapterMessages.updateMessages(messages);
                rvMessages.scrollToPosition(messages.size()-1);
            }
        };
        int idSender = getIdSender();
        if(idSender == 0) finish();//TODO hacer control de errores
        chatClient.startSession(idSender, id);
        findViewById(R.id.sendMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!text.getText().toString().equals("")){
                    Message message = new Message(text.getText().toString(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")), false);
                    chatClient.sendMessage(message);
                    Log.e("S", ""+message.isMine());
                    message.setMine(true);
                    messages.add(message);
                    Log.e("M", ""+message.isMine());
                    adapterMessages.updateMessages(messages);
                    rvMessages.scrollToPosition(messages.size()-1);
                    text.setText("");
                }
            }
        });
        findViewById(R.id.backArrowChat).setOnClickListener((View v) -> {
            chatClient.closeSession();
            finish();
        });
    }

    private int getIdSender() {
        String namePrefs = "BatoiPopPrefs";
        SharedPreferences myPreferences = null;
        try {
            myPreferences = EncryptedSharedPreferences.create(namePrefs,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    getApplicationContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String usuarioStr = myPreferences.getString("usuarioLogged", null);
        if(myPreferences!=null && usuarioStr !=null){
            Gson gson = new Gson();
            BatoipopUsuario user = gson.fromJson(usuarioStr, BatoipopUsuario.class);
            return user.getId();
        }
        return 0;
    }
}