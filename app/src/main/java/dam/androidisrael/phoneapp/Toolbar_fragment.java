package dam.androidisrael.phoneapp;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Toolbar_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Toolbar_fragment extends Fragment {

    public interface FragmentPickerListener {
        void setActivityHome();
        void setFavoritesFragment();
        void setAddNewProductFragment();
        void setChatsFragment();
        void setProfileFragment();
    }

    public FragmentPickerListener listener;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Toolbar_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        listener = (FragmentPickerListener) context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment toolbar_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Toolbar_fragment newInstance(String param1, String param2) {
        Toolbar_fragment fragment = new Toolbar_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_toolbar_fragment, container, false);

        view.findViewById(R.id.buttonAddProduct2).setOnClickListener(view1 -> listener.setAddNewProductFragment());
        view.findViewById(R.id.buttonHome2).setOnClickListener(view2 -> listener.setActivityHome());
        view.findViewById(R.id.buttonFavorites2).setOnClickListener(view3 -> listener.setFavoritesFragment());
        view.findViewById(R.id.buttonChats2).setOnClickListener(view4 -> listener.setChatsFragment());
        view.findViewById(R.id.buttonProfile2).setOnClickListener(view5 -> listener.setProfileFragment());

        return view;
    }
}