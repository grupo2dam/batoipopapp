package dam.androidisrael.phoneapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import dam.androidisrael.phoneapp.api.CreateEtiqueta;
import dam.androidisrael.phoneapp.api.CreateProduct;
import dam.androidisrael.phoneapp.api.GetCategorias;
import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;
import dam.androidisrael.phoneapp.pojos.BatoipopCategoria;
import dam.androidisrael.phoneapp.pojos.BatoipopEtiqueta;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_nuevo_producto#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_nuevo_producto extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public static final int PICK_IMAGE = 1;
    private ImageView imgSubir;
    private ImageView imgIcon, imgAdd;
    private TextView tvSubir;
    private ChipGroup cgEtiquetas;
    private Button btAdd;
    ArrayList<BatoipopCategoria> categoriasUsu = new ArrayList<>();
    String[] categoriaStr = new String[]{};

    private EditText etEtiquetas;

    public Fragment_nuevo_producto() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_nuevo_producto.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_nuevo_producto newInstance(String param1, String param2) {
        Fragment_nuevo_producto fragment = new Fragment_nuevo_producto();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nuevo_producto, container, false);

        Spinner spinner = view.findViewById(R.id.spinnerCategoria);
        EditText etNombreProducto = view.findViewById(R.id.etNombreProducto);
        EditText etDesc = view.findViewById(R.id.etDesc);
        EditText etPrecio = view.findViewById(R.id.etPrecio);
        EditText etAntiguedad = view.findViewById(R.id.etAntiguedad);


        GetCategorias getCategorias = new GetCategorias() {
            @Override
            public void OnFinish(ArrayList<BatoipopCategoria> categorias) {
                categoriasUsu = categorias;
                categoriaStr = new String[categorias.size()];
                for (int i = 0; i < categorias.size(); i++) {
                    categoriaStr[i] = categorias.get(i).getNombre();
                }
                spinner.setAdapter(new ArrayAdapter<String>(view.getContext(), R.layout.spinner_item, categoriaStr));
            }
        };
        getCategorias.get();

        imgSubir = view.findViewById(R.id.imgSubir);
        imgIcon = view.findViewById(R.id.idIcono);
        tvSubir = view.findViewById(R.id.tvSubir);
        imgAdd = view.findViewById(R.id.imgAdd);
        cgEtiquetas = view.findViewById(R.id.cgEtiquetas);

        Button btAddProduct = view.findViewById(R.id.btCreateProduct);
        btAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getUsuario()==null) {
                    Toast.makeText(getContext(), "You must be logged in to upload a new product", Toast.LENGTH_LONG).show();
                    return;
                }
                ProgressDialog progressDialog = new ProgressDialog(view.getContext());
                progressDialog.setMessage("Loading Products");
                CreateProduct createProduct = new CreateProduct() {
                    @Override
                    public void OnFinish(boolean hasBeenCreated) {
                        if(hasBeenCreated){
                            Toast.makeText(getContext(), "Product Created", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext(), "Error creating Product", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                };
                BatoipopArticulo art = new BatoipopArticulo();
                if(!etNombreProducto.getText().toString().equals("")&&!etPrecio.getText().toString().equals("")){
                    art.setNombre(etNombreProducto.getText().toString());
                    art.setBatoipopCategoria(categoriasUsu.get(spinner.getSelectedItemPosition()));
                    System.out.println("spinnner"+spinner.getSelectedItemPosition());
                    art.setPrecio(Float.parseFloat(etPrecio.getText().toString()));
                    art.setDescripcion(etDesc.getText().toString());
                    art.setAntiguedad(etAntiguedad.getText().toString());
                    art.setUsuarioPublicar(getUsuario());
                    Set<BatoipopEtiqueta> etiquetas = new HashSet<>();
                    //Handler handler = new Handler(Looper.getMainLooper());
                    AtomicInteger etiquetasSubidas = new AtomicInteger(0);
                    CreateEtiqueta createEtiqueta = new CreateEtiqueta() {
                        @Override
                        public void OnFinish(boolean hasWorked, BatoipopEtiqueta etiqueta) {
                            if(hasWorked){
                                etiquetas.add(etiqueta);
                                etiquetasSubidas.getAndIncrement();
                            }
                        }
                    };
                    progressDialog.show();
                    for (int i = 0; i < cgEtiquetas.getChildCount(); i++) {
                        String etiqueta = ((Chip) cgEtiquetas.getChildAt(i)).getText().toString();
                        createEtiqueta.create(etiqueta);
                    }
                    Thread threadCreate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean continueW = true;
                            while(continueW){
                                Log.i("ET", etiquetasSubidas.get()+"/"+cgEtiquetas.getChildCount());
                                if(etiquetasSubidas.get() == cgEtiquetas.getChildCount()){
                                    art.setBatoipopEtiquetas(etiquetas);
                                    Log.e("PRODUCT", art.getBatoipopEtiquetas().size()+"");
                                    createProduct.create(art);
                                    continueW = false;
                                }
                            }
                        }
                    });
                    threadCreate.start();

                } else {
                    Toast.makeText(getContext(), "The name and price of the product must be set", Toast.LENGTH_LONG).show();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

        builder.setView(View.inflate(view.getContext(), R.layout.tag_add, null));

        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.show();

                btAdd = dialog.findViewById(R.id.bt_add);
                etEtiquetas = dialog.findViewById(R.id.et_etiqueta);

                btAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Chip c = new Chip(view.getContext());
                        c.setText(etEtiquetas.getText());
                        c.setTextSize(12);
                        c.setTextColor(Color.WHITE);
                        c.setCloseIconTintResource(R.color.white);
                        int[] colors = new int[] {
                                R.color.orange,
                                R.color.blue1,
                                R.color.green
                        };
                        c.setCloseIconEnabled(true);
                        c.setOnCloseIconClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                cgEtiquetas.removeView(c);
                            }

                        });
                        c.setChipBackgroundColorResource(colors[(int) (Math.random() * colors.length)]);
                        dialog.hide();
                        if(!etEtiquetas.getText().equals("")){
                            cgEtiquetas.addView(c);
                        }
                        etEtiquetas.setText("");
                    }

                });

            }
        });

        imgSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });
        return view;
    }

    private BatoipopUsuario getUsuario() {
            String namePrefs = "BatoiPopPrefs";
            SharedPreferences myPreferences = null;
            String userLogged = null;
            try {
                myPreferences = EncryptedSharedPreferences.create(namePrefs,
                        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                        getContext(),
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
                userLogged = myPreferences.getString("usuarioLogged", null);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(userLogged!=null){
                Gson gson = new Gson();
                return gson.fromJson(userLogged, BatoipopUsuario.class);
            }
            return null;
    }

    private void selectImageFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE) {
            if (data != null){
                Uri uri = data.getData();
                imgSubir.setImageURI(uri);
                imgSubir.setVisibility(View.VISIBLE);
                imgIcon.setVisibility(View.INVISIBLE);
                tvSubir.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation a = new Animation() {};
        a.setDuration(0);
        return a;
        //return super.onCreateAnimation(transit, enter, nextAnim);
    }
}