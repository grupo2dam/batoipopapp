package dam.androidisrael.phoneapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;

import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public class EditarUsuario extends AppCompatActivity {
    public static final int PICK_IMAGE = 1;
    ImageView imgUser;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_usuario);

        setUI();
    }

    private void setUI() {
        getSupportActionBar().hide();
        imgUser = findViewById(R.id.imgUsuario);
        EditText etName = findViewById(R.id.etNombre);
        fab = findViewById(R.id.floatingActionButton);
        ImageView backArrow = findViewById(R.id.backArrow2);
        backArrow.setOnClickListener((View v) -> { finish(); });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });
        String namePrefs = "BatoiPopPrefs";
        SharedPreferences myPreferences = null;
        try {
             myPreferences = EncryptedSharedPreferences.create(namePrefs,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    getApplicationContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String usuarioStr = myPreferences.getString("usuarioLogged", null);
        if(myPreferences!=null && usuarioStr !=null){
            Gson gson = new Gson();
            BatoipopUsuario user = gson.fromJson(usuarioStr, BatoipopUsuario.class);
            etName.setText(user.getNombre());
        }

    }

    private void selectImageFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                Uri uri = data.getData();
                imgUser.setImageURI(uri);

            }
        }
    }
}