package dam.androidisrael.phoneapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import dam.androidisrael.phoneapp.api.GetCategorias;
import dam.androidisrael.phoneapp.pojos.BatoipopCategoria;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment {
    Filter filter = new Filter() {
        @Override
        public void OnFilterPrice(String min, String max) {}
        @Override
        public void OnFilterEtiquetas(String nombre) {}
        @Override
        public void OnFilterCategorias(String nombre) {}
    };

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFragment newInstance(String param1, String param2) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        EditText min = view.findViewById(R.id.etNumMin);
        EditText max = view.findViewById(R.id.etNumMax);
        Spinner spinnerCat = view.findViewById(R.id.spinnerCategoria2);
        EditText etEtiquetas = view.findViewById(R.id.etEtiquetas);
        GetCategorias getCategorias = new GetCategorias() {
            @Override
            public void OnFinish(ArrayList<BatoipopCategoria> categorias) {
                String[] categoriaStr = new String[categorias.size()+1];
                categoriaStr[0] = "TODAS";
                for (int i = 1; i < categorias.size()+1; i++) {
                    categoriaStr[i] = categorias.get(i-1).getNombre();
                }
                spinnerCat.setAdapter(new ArrayAdapter<String>(view.getContext(), R.layout.spinner_item, categoriaStr));
                spinnerCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        filter.OnFilterCategorias(categoriaStr[position]);
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) { }
                });
            }
        };
        getCategorias.get();
        etEtiquetas.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                filter.OnFilterEtiquetas(s.toString());
            }
        });
        min.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String maxim = max.getEditableText().toString();
                String minim = s.toString();
                if(maxim==null || maxim.equals("")) maxim = String.valueOf(Integer.MAX_VALUE);
                if(minim==null || minim.equals("")) minim = String.valueOf(Integer.MIN_VALUE);
                filter.OnFilterPrice(minim, maxim);
            }
        });
        max.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String maxim = s.toString();
                String minim = min.getText().toString();
                if(maxim==null || maxim.equals("")) maxim = String.valueOf(Integer.MAX_VALUE);
                if(minim==null || minim.equals("")) minim = String.valueOf(Integer.MIN_VALUE);
                filter.OnFilterPrice(minim, maxim);
            }
        });
        return view;
    }

    interface Filter{
        void OnFilterPrice(String min, String max);
        void OnFilterEtiquetas(String nombre);
        void OnFilterCategorias(String nombre);
    }
}