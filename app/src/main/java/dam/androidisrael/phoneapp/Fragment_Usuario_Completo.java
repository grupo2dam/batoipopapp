package dam.androidisrael.phoneapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainer;
import androidx.fragment.app.FragmentContainerView;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;

import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_Usuario_Completo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_Usuario_Completo extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Fragment_Usuario_Completo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_Usuario_Completo.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_Usuario_Completo newInstance(String param1, String param2) {
        Fragment_Usuario_Completo fragment = new Fragment_Usuario_Completo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //Fragment_Opcion fragmentOpcion = Fragment_Opcion.newInstance("Vendidios", R.drawable.action_hero);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment__usuario__completo, container, false);
        ImageView imgVentas = view.findViewById(R.id.imgVentas);
        ConstraintLayout clVentas = view.findViewById(R.id.clVentas);
        clVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ConstraintLayout clAjustes = view.findViewById(R.id.clAjustes);
        clAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(view.getContext(), EditarUsuario.class));
            }
        });
        TextView tvName = view.findViewById(R.id.tvNombre);
        String namePrefs = "BatoiPopPrefs";
        SharedPreferences myPreferences = null;
        try {
            myPreferences = EncryptedSharedPreferences.create(namePrefs,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    view.getContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String usuarioStr = myPreferences.getString("usuarioLogged", null);
        if(myPreferences!=null && usuarioStr !=null){
            Gson gson = new Gson();
            BatoipopUsuario user = gson.fromJson(usuarioStr, BatoipopUsuario.class);
            tvName.setText(user.getNombre());
        }
        return  view;
    }

    @Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation a = new Animation() {};
        a.setDuration(0);
        return a;
        //return super.onCreateAnimation(transit, enter, nextAnim);
    }
}