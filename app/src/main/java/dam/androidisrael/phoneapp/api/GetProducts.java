package dam.androidisrael.phoneapp.api;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;
import dam.androidisrael.phoneapp.pojos.BatoipopCategoria;
import dam.androidisrael.phoneapp.pojos.BatoipopEtiqueta;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public abstract class GetProducts {
    private ArrayList<BatoipopArticulo> articulos = new ArrayList<>();

    public void get(){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://137.74.226.42:8080/articulo/all");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    getArticulos(urlConnection, articulos);
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(articulos.size()>0){
                            OnFinish(articulos);
                        }
                    }
                });
            }
        });
    }

    private void getArticulos(HttpURLConnection urlConnection, ArrayList<BatoipopArticulo> articulos) {
        try {
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String resultStream = readStream(urlConnection.getInputStream());
                Log.i("RESULTSTREAM_ART", resultStream);

                JSONArray jArray = new JSONArray(resultStream);
                if (jArray.length() > 0) {
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject item = jArray.getJSONObject(i);
                        BatoipopArticulo articulo = new BatoipopArticulo();
                        articulo.setId(item.getInt("id"));
                        articulo.setNombre(item.getString("nombre"));
                        articulo.setDescripcion(item.getString("descripcion"));
                        articulo.setAntiguedad(item.getString("antiguedad"));
                        articulo.setPrecio((float) item.getDouble("precio"));
                        Set<BatoipopEtiqueta> batoipopEtiquetas = new HashSet<>();
                        articulo.setUsuarioPublicar(getBatoipopUsuario(item));
                        articulo.setBatoipopCategoria(getCategoria(item));
                        JSONArray etiquetas = item.getJSONArray("batoipopEtiquetas");
                        for (int j = 0; j < etiquetas.length(); j++) {
                            JSONObject etiqueta = etiquetas.getJSONObject(j);
                            BatoipopEtiqueta batoipopEtiqueta = new BatoipopEtiqueta();
                            batoipopEtiqueta.setId(etiqueta.getInt("id"));
                            batoipopEtiqueta.setNombre(etiqueta.getString("nombre"));
                            batoipopEtiquetas.add(batoipopEtiqueta);
                        }
                        articulo.setBatoipopEtiquetas(batoipopEtiquetas);
                        articulos.add(articulo);
                        Log.i("ARTICLE", "Y");
                    }
                } else {

                }
            } else {
                Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            }
        } catch (JSONException e) {
            Log.i("URL", "ErrorCode: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BatoipopCategoria getCategoria(JSONObject item) {
        try {
            JSONObject categoria = item.getJSONObject("batoipopCategoria");
            BatoipopCategoria batoipopCategoria = new BatoipopCategoria();
            batoipopCategoria.setId(categoria.getInt("id"));
            batoipopCategoria.setNombre(categoria.getString("nombre"));
            return batoipopCategoria;
        } catch (JSONException e) {
            Log.e("GETPRODUCTS", "ARTICULO SIN CATEGORIA");
            return null;
        }
    }

    private BatoipopUsuario getBatoipopUsuario(JSONObject item) {
        try {
            JSONObject usuario = item.getJSONObject("usuarioPublicar");
            BatoipopUsuario batoipopUsuario = new BatoipopUsuario();
            batoipopUsuario.setId(usuario.getInt("id"));
            batoipopUsuario.setNombre(usuario.getString("nombre"));
            batoipopUsuario.setEmail(usuario.getString("email"));
            batoipopUsuario.setTelefono(usuario.getString("telefono"));
            return batoipopUsuario;
        } catch (JSONException e) {
            Log.e("GETPRODUCTS", "ARTICULO SIN USUARIO");
            return null;
        }
    }


    private String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";
        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }

    public abstract void OnFinish(ArrayList<BatoipopArticulo> articulos);
}
