package dam.androidisrael.phoneapp.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public abstract class SignUpUsuario {
    private BatoipopUsuario usuario;

    public void Login(String user, String pass, String mail, String telefono){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            URL url1;
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://137.74.226.42:8080/usuario");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    String jsonInputString = "{" +
                            "\"nombre\":\"" + user + "\"," +
                            "\"email\":\"" + mail + "\"," +
                            "\"telefono\":\"" + telefono + "\"," +
                            "\"contrasena\":\"" + pass + "\"" +
                            "}";

                    try(OutputStream os = urlConnection.getOutputStream()) {
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                        Log.d("RESPONSE", "ok");
                    }
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();

                    Log.d("url", String.valueOf(urlConnection.getResponseCode()));
                    //String sessionId = getSessionId(urlConnection);

                    //url1 = new URL("http://137.74.226.42:8069/web/image?model=batoipop.articulo&field=foto&session_id="+sessionId+"&id=");
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                            OnFinish(usuario);
                    }
                });
            }
        });
    }

    public abstract void OnFinish(BatoipopUsuario usuario);
}
