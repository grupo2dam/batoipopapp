package dam.androidisrael.phoneapp.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public abstract class GetLoginUsuario {
    private BatoipopUsuario usuario = null;

    public void login(String user, String pass){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://137.74.226.42:8080/usuario/login?u=" + user + "&p=" + pass);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    getUsuario(urlConnection);
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OnFinish(usuario);
                    }
                });
            }
        });
    }

    private void getUsuario(HttpURLConnection urlConnection) {
        try {
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                usuario = new BatoipopUsuario();
                String resultStream = readStream(urlConnection.getInputStream());
                Log.i("RESULTSTREAM", resultStream);

                JSONObject jObject = new JSONObject(resultStream);
                usuario.setId(jObject.getInt("id"));
                usuario.setNombre(jObject.getString("nombre"));
                usuario.setEmail(jObject.getString("email"));
                usuario.setTelefono("telefono");
                usuario.setContrasena(jObject.getString("contrasena"));
                Log.d("USUARIO", "OK");
            } else {
                Log.e("URL", "ErrorCode: " + urlConnection.getResponseCode());
                usuario = null;
            }
        } catch (JSONException e) {
            Log.i("URL", " " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";
        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }

    public abstract void OnFinish(BatoipopUsuario usuario);
}
