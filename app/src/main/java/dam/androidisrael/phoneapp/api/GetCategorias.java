package dam.androidisrael.phoneapp.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.pojos.BatoipopCategoria;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public abstract class GetCategorias {
    ArrayList<BatoipopCategoria> categorias = new ArrayList<>();

    public void get(){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://137.74.226.42:8080/categoria/all");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    getCategorias(urlConnection);
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OnFinish(categorias);
                    }
                });
            }
        });
    }

    private void getCategorias(HttpURLConnection urlConnection) {
        try {
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String resultStream = readStream(urlConnection.getInputStream());
                Log.i("RESULTSTREAM", resultStream);

                JSONArray jsonArray = new JSONArray(resultStream);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject catjson = jsonArray.getJSONObject(i);
                    BatoipopCategoria categoria = new BatoipopCategoria();
                    categoria.setId(catjson.getInt("id"));
                    categoria.setNombre(catjson.getString("nombre"));
                    categorias.add(categoria);
                }

                Log.d("USUARIO", "OK");
            } else {
                Log.e("URL", "ErrorCode: " + urlConnection.getResponseCode());
            }
        } catch (JSONException e) {
            Log.i("URL", " " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";
        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }

    public abstract void OnFinish(ArrayList<BatoipopCategoria> categorias);
}
