package dam.androidisrael.phoneapp.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;

public abstract class CreateProduct {
    HttpURLConnection urlConnection = null;
    boolean hasWorked = false;

    public void create(BatoipopArticulo articulo){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://137.74.226.42:8080/articulo");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    String jsonInputString = getJsonString(articulo);
                    Log.i("JSONNEWPRODUCT", jsonInputString);
                    try(OutputStream os = urlConnection.getOutputStream()) {
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    Log.i("JSONNEWPRODUCT_RESPONSE", urlConnection.getResponseCode()+"");
                    hasWorked = urlConnection.getResponseCode() == HttpURLConnection.HTTP_CREATED;

                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OnFinish(hasWorked);
                    }
                });
            }
        });
    }

    private String getJsonString(BatoipopArticulo product){
        Gson gson = new Gson();
        return gson.toJson(product);
    };

    public abstract void OnFinish(boolean hasBeenCreated);
}
