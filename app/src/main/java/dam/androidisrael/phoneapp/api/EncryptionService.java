package dam.androidisrael.phoneapp.api;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionService {
    private  SecretKey secretKey;
    private  Cipher cipher = null;
    private static EncryptionService encryptionService = null;

    public static EncryptionService getIntance(){
        if(encryptionService == null){
            return new EncryptionService();
        }
        return encryptionService;
    }

    private EncryptionService() {
        try {
            byte[ ] decodekey = "grupo2damproyect".getBytes(StandardCharsets.UTF_8);
            this.secretKey = new SecretKeySpec(decodekey, 0 , decodekey.length, "AES");
            cipher = Cipher.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public String encrypt(String plainText) throws Exception {
        byte[] plainTextByte = plainText.getBytes ("UTF-8");
        // encrypt
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        // convert to string
        String encryptedText = Base64.getEncoder().encodeToString(encryptedByte);
        return encryptedText;
    }
    public String decrypt(String encryptedText) throws Exception {
        byte[] encryptedTextByte = Base64.getDecoder().decode(encryptedText);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte, "UTF-8");
        return decryptedText;
    }
}
