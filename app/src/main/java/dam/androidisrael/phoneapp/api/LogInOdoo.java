package dam.androidisrael.phoneapp.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class LogInOdoo {

    public void Login(){
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(new Runnable() {
            URL url1;
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://137.74.226.42:8069/web/session/authenticate");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    String jsonInputString = "{" +
                            "    \"jsonrpc\":\"2.0\"," +
                            "    \"params\":{\"db\": \"batoipop\"," +
                            "            \"login\": \"grupo2dam@proyecto.com\"," +
                            "            \"password\": \"grupo2dam\"}" +
                            "}";
                    try(OutputStream os = urlConnection.getOutputStream()) {
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();
                    String sessionId = getSessionId(urlConnection);
                    Log.d("RESPONSE", sessionId);
                    url1 = new URL("http://137.74.226.42:8069/web/image?model=batoipop.articulo&field=foto&session_id="+sessionId+"&id=");
                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                            OnFinish(url1.toString());
                    }
                });
            }
        });
    }

    private String getSessionId(HttpURLConnection urlConnection) {
        Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
        Set<String> headerFieldsSet = headerFields.keySet();
        Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();

        while (hearerFieldsIter.hasNext()) {
            String headerFieldKey = hearerFieldsIter.next();
            if ("Set-Cookie".equalsIgnoreCase(headerFieldKey)) {
                List<String> headerFieldValue = headerFields.get(headerFieldKey);
                String[] fields = headerFieldValue.get(0).split(";");
                String cookieValue = fields[0];
                return cookieValue.split("=")[1];
            }
        }
        return null;
    }

    public abstract void OnFinish(String url);
}
