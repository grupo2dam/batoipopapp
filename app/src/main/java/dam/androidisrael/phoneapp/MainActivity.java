package dam.androidisrael.phoneapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import dam.androidisrael.phoneapp.api.GetLoginUsuario;
import dam.androidisrael.phoneapp.api.GetProducts;
import dam.androidisrael.phoneapp.api.LogInOdoo;
import dam.androidisrael.phoneapp.api.SignUpUsuario;
import dam.androidisrael.phoneapp.pojos.BatoipopArticulo;
import dam.androidisrael.phoneapp.pojos.BatoipopEtiqueta;
import dam.androidisrael.phoneapp.pojos.BatoipopUsuario;

public class MainActivity extends AppCompatActivity implements Toolbar_fragment.FragmentPickerListener {
    private MyAdapter adapter;
    private RecyclerView rvProductos;
    private ArrayList<BatoipopArticulo> productos;
    private int fragmentNumber = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.fcMenu, fragment, null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
        findViewById(R.id.fcMenu).setVisibility(View.VISIBLE);
    }

    private void setUI() {
        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading Products");
        progressDialog.show();
        LogInOdoo logInOdoo = new LogInOdoo() {
            @Override
            public void OnFinish(String url) {
                GetProducts getProducts = new GetProducts() {
                    @Override
                    public void OnFinish(ArrayList<BatoipopArticulo> articulos) {
                        productos = articulos;
                        setAdapter(url , articulos);
                        progressDialog.dismiss();
                    }
                };
                getProducts.get();
            }
        };
        logInOdoo.Login();

        getSupportActionBar().hide();
        rvProductos = findViewById(R.id.rvMessages);
        EditText etSearch = findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                filterByName(s.toString());
            }
        });
        ImageView filter = findViewById(R.id.filter);
        prepareFilter();
        FragmentContainerView fcFilter = findViewById(R.id.fcFilter);
        Log.i("--------------------------------------------------------","D:"+fcFilter.getChildCount());
        fcFilter.setVisibility(View.GONE);
        filter.setOnClickListener((View v) -> {
            if (fcFilter.getVisibility() == View.VISIBLE) {
                TransitionManager.beginDelayedTransition(fcFilter,
                        new Slide(Gravity.START));
                fcFilter.setVisibility(View.GONE);
            }
            else {
                TransitionManager.beginDelayedTransition(fcFilter,
                       new Slide(Gravity.START));
                fcFilter.setVisibility(View.VISIBLE);
            }
        });

        rvProductos.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        rvProductos.addItemDecoration(new SpacesItemDecoration(10));
    }

    private void prepareFilter() {
        FilterFragment fragment = new FilterFragment();
        fragment.setFilter(new FilterFragment.Filter() {
            @Override
            public void OnFilterPrice(String min, String max) {
                filterByPrice(min, max);
            }
            @Override
            public void OnFilterEtiquetas(String nombre) {
                filterByEtiqueta(nombre);
            }
            @Override
            public void OnFilterCategorias(String nombre) {
                filterByCategoria(nombre);
            }
        });
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.fcFilter, fragment, null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void filterByCategoria(String nombre) {
        ArrayList<BatoipopArticulo> articulos = new ArrayList<>();
        System.out.println("NOMBREFILTROCATEGORIA"+": "+nombre);
        if(nombre.equals("TODAS")){
            adapter.updateProducts(this.productos);
            return;
        }
        if(productos!=null){
            for (BatoipopArticulo producto : productos) {
                System.out.println(producto.getBatoipopCategoria());
                if(producto.getBatoipopCategoria()!=null){
                    if(producto.getBatoipopCategoria().getNombre().equalsIgnoreCase(nombre)){
                        articulos.add(producto);
                    }
                }
            }
            adapter.updateProducts(articulos);
        }
    }

    private void filterByEtiqueta(String nombre) {
        ArrayList<BatoipopArticulo> articulos = new ArrayList<>();
        if(nombre.equals("")){
            adapter.updateProducts(this.productos);
            return;
        }
        if(productos!=null){
            for (BatoipopArticulo producto : productos) {
                if(producto.getBatoipopEtiquetas().size()!=0){
                    for (BatoipopEtiqueta batoipopEtiqueta : producto.getBatoipopEtiquetas()) {
                        if(batoipopEtiqueta.getNombre().toLowerCase().contains(nombre.toLowerCase())){
                            articulos.add(producto);
                        }
                    }
                }
            }
            adapter.updateProducts(articulos);
        }
    }

    private void filterByName(String search) {
        ArrayList<BatoipopArticulo> articulos = new ArrayList<>();
        if(productos!=null){
            for (BatoipopArticulo producto : productos) {
                if(producto.getNombre().toLowerCase().contains(search.toLowerCase())){
                    articulos.add(producto);
                }
            }
            adapter.updateProducts(articulos);
        }
    }

    private void filterByPrice(String min, String max) {
        int maxInt = Integer.parseInt(max);
        int minInt = Integer.parseInt(min);
        ArrayList<BatoipopArticulo> articulos = new ArrayList<>();
        for (BatoipopArticulo producto : productos) {
            if(producto.getPrecio()>=minInt & producto.getPrecio()<=maxInt){
                articulos.add(producto);
            }
        }
        adapter.updateProducts(articulos);
    }

    @Override
    public void setActivityHome() {
        if (fragmentNumber != 0) {
            findViewById(R.id.fcMenu).setVisibility(View.INVISIBLE);
            fragmentNumber = 0;
        }
    }

    @Override
    public void setFavoritesFragment() {

    }

    @Override
    public void setAddNewProductFragment() {
        if (fragmentNumber != 2) {
            Fragment fragmentAddNewProduct = new Fragment_nuevo_producto();
            replaceFragment(fragmentAddNewProduct);
            fragmentNumber = 2;
        }
    }

    @Override
    public void setChatsFragment() {
        if (fragmentNumber != 3) {
            Fragment fragmentUserChat = new ChatsUserFragment();
            replaceFragment(fragmentUserChat);
            fragmentNumber = 3;
        }
    }

    @Override
    public void setProfileFragment() {
        String namePrefs = "BatoiPopPrefs";
        SharedPreferences myPreferences = null;
        try {
            myPreferences = EncryptedSharedPreferences.create(namePrefs,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    getApplicationContext(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(myPreferences!=null && myPreferences.getString("usuarioLogged", null)!=null){
            seeProfile();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme));

            builder.setView(View.inflate(getApplicationContext(), R.layout.usuario_login, null));

            builder.setCancelable(true);
            AlertDialog dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setLoginDialog(dialog);
        }
    }

    private void seeProfile() {
        if (fragmentNumber != 4) {
            Fragment fragmentProfile = new Fragment_Usuario_Completo();
            replaceFragment(fragmentProfile);
            fragmentNumber = 4;
        }
    }

    private void setLoginDialog(AlertDialog dialog) {
        dialog.show();

        Button btAdd = dialog.findViewById(R.id.bt_login);
        EditText etuser = dialog.findViewById(R.id.et_user);
        EditText etpass = dialog.findViewById(R.id.et_pass);
        Button btSignUp = dialog.findViewById(R.id.bt_singup1);

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.AppTheme));

                builder.setView(View.inflate(view.getContext(), R.layout.usuario_sign_up, null));

                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                Button btsignup = dialog.findViewById(R.id.bt_singup1);
                EditText user = dialog.findViewById(R.id.et_user);
                EditText pass = dialog.findViewById(R.id.et_pass);
                EditText mail = dialog.findViewById(R.id.et_mail);
                EditText telf = dialog.findViewById(R.id.et_telf);

                btsignup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!user.getText().toString().equals("")&&!pass.getText().toString().equals("")){
                            System.out.println("USER CREATED"+user);
                            SignUpUsuario signUpUsuario = new SignUpUsuario() {
                                @Override
                                public void OnFinish(BatoipopUsuario usuario) {
                                    Toast.makeText(getApplicationContext(), "User Created", Toast.LENGTH_LONG).show();
                                    dialog.hide();
                                }
                            };
                            signUpUsuario.Login(String.valueOf(user.getText()), String.valueOf(pass.getText()), String.valueOf(mail.getText()), String.valueOf(telf.getText()));
                        } else {
                            Toast.makeText(getApplicationContext(), "The username and password must be set", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetLoginUsuario getUsuario = new GetLoginUsuario() {
                    @Override
                    public void OnFinish(BatoipopUsuario usuario) {
                        if(usuario!=null){
                            String namePrefs = "BatoiPopPrefs";
                            SharedPreferences myPreferences = null;
                            try {
                                myPreferences = EncryptedSharedPreferences.create(namePrefs,
                                        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                                        view.getContext(),
                                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
                            } catch (GeneralSecurityException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(myPreferences!=null){
                                SharedPreferences.Editor editor = myPreferences.edit();
                                Gson gson = new Gson();
                                editor.putString("usuarioLogged", gson.toJson(usuario));
                                editor.commit();
                                dialog.hide();
                                seeProfile();
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "Error Logging in", Toast.LENGTH_LONG);
                                toast.show();
                            }

                        } else {
                            Toast toast = Toast.makeText(getApplicationContext(), "The user doesn't exist or the credentials are wrong", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                };
                getUsuario.login(String.valueOf(etuser.getText()), String.valueOf(etpass.getText()));
            }
        });
    }

    private void setAdapter(String url, ArrayList<BatoipopArticulo> articulos){
        Log.i("LAST REQUEST", url);
        ArrayList<BatoipopArticulo> products = new ArrayList<>();

        for (BatoipopArticulo articulo : articulos) {
            articulo.setImageURL(url+articulo.getId());
            products.add(articulo);
        }

        adapter = new MyAdapter(products);
        rvProductos.setAdapter(adapter);
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;

            if ((parent.getChildCount() > 0 && parent.getChildPosition(view) == 0)
                    || (parent.getChildCount() > 1 && parent.getChildPosition(view) == 1))
                outRect.top = space;
        }
    }
}