package dam.androidisrael.phoneapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dam.androidisrael.phoneapp.models.Usuario;

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder> {
    private List<Usuario> valoraciones;
    private LayoutInflater layoutInflater;

    public AdapterChat(List<Usuario> valoraciones, Context context) {
        this.valoraciones = valoraciones;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.card_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(valoraciones.get(position));
    }

    @Override
    public int getItemCount() {
        return valoraciones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre, tvComentario;
        ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvComentario = itemView.findViewById(R.id.tvComentario);
            img = itemView.findViewById(R.id.imgUsuario);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext().getApplicationContext(), UserChat.class);
                    intent.putExtra("id", 1);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
        void bindData(final Usuario item){
            tvNombre.setText(item.getNombre());
            tvComentario.setText(item.getNombre());
        }
    }
}
