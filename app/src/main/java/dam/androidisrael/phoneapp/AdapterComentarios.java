package dam.androidisrael.phoneapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dam.androidisrael.phoneapp.models.Valoracion;

public class AdapterComentarios extends RecyclerView.Adapter<AdapterComentarios.ViewHolder> {
    private List<Valoracion> valoraciones;
    private LayoutInflater layoutInflater;

    public AdapterComentarios(List<Valoracion> valoraciones, Context context) {
        this.valoraciones = valoraciones;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.valoraciones, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(valoraciones.get(position));
    }

    @Override
    public int getItemCount() {
        return valoraciones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre, tvComentario;
        ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvComentario = itemView.findViewById(R.id.tvComentario);
            img = itemView.findViewById(R.id.imgUsuario);
        }
        void bindData(final Valoracion item){
            tvNombre.setText(item.getUsuario_comentario().getNombre());
            tvComentario.setText(item.getContenido());
        }
    }
}
